#!/bin/sh

# perf-flame
#
# A script that converts output from perf-record into a flamegraph SVG.
# Perf-record output is gzipped output of 'perf script'.
#
# This script depends on stackcollapse-perf.pl and flamegraph.pl from
# https://github.com/brendangregg/FlameGraph. These two scripts must be
# available in $PATH.

if [ $# -lt 1 ] || [ $# -gt 2 ]; then
  echo 'Usage: perf-flame PERF_SCRIPT_GZ'
  exit 1
fi

set -xe

stat -- "$1">/dev/null

gzip -d < "$1" |\
  stackcollapse-perf.pl |\
  flamegraph.pl --title "$1" --hash > "$1.svg"
open "$1.svg"
