#!/bin/sh

# pprof-record records a set of Go pprof profiles on either localhost or
# a remote machine using curl. Remote profile capture uses SSH. The
# default profile time is 30 seconds but you can override that by
# setting TIME.

case $# in
1)
  host="$(hostname)"
  run=""
  ;;
2)
  host="$2"
  run="ssh $host"
  ;;
*)
  echo 'Usage: pprof-record PPROF_PORT [HOST]'
  exit 1
  ;;
esac

set -e

port="$1"
name="$host-$port-$(date -u '+%Y-%m-%d_%H%M')-$(hexdump -e '"%x"' -v -n4 /dev/random).pprof"
$run /bin/sh -s -- "$port" "$name" "${TIME:-30}" <<'EOF' | tar x
  set -ex
  cd /tmp
  mkdir -- "$2"
  trap "rm -rf -- $2" EXIT
  (
    cd "$2"
    curl -o allocs.t0 --silent http://localhost:$1/debug/pprof/allocs
    curl -o cpu --silent "http://localhost:$1/debug/pprof/profile?seconds=$3"
    curl -o allocs.t1 --silent http://localhost:$1/debug/pprof/allocs
    curl -o heap --silent http://localhost:$1/debug/pprof/heap
  )
  tar c "$2"
EOF
