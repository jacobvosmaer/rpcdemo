/*
 *
 * Copyright 2015 gRPC authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package main

import (
	"context"
	"flag"
	"io"
	"log"
	"os"
	"path"
	"runtime/pprof"
	"time"

	pb "rpcdemo/helloworld/helloworld"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

const (
	defaultName = "world"
)

var (
	addr     = flag.String("addr", "localhost:50051", "the address to connect to")
	total    = flag.Int("total", 1<<20, "total data size")
	block    = flag.Int("block", 1<<12, "block size")
	pprofDir = flag.String("pprof", "", "base name for pprof output")
)

func createPprofFile(filename string) (*os.File, error) {
	if err := os.MkdirAll(*pprofDir, 0755); err != nil {
		return nil, err
	}
	return os.Create(path.Join(*pprofDir, filename))
}

func writeProfileSnapshot(profileType string, filename string) error {
	f, err := createPprofFile(filename)
	if err != nil {
		return err
	}
	defer f.Close()
	if err := pprof.Lookup(profileType).WriteTo(f, 0); err != nil {
		return err
	}
	return f.Close()
}

func main() {
	flag.Parse()

	if *pprofDir != "" {
		*pprofDir += "-" + time.Now().Format(time.RFC3339) + ".pprof"
		log.Printf("storing pprof files in %s", *pprofDir)

		if err := writeProfileSnapshot("allocs", "allocs.t0"); err != nil {
			log.Fatal(err)
		}
		defer func() {
			if err := writeProfileSnapshot("allocs", "allocs.t1"); err != nil {
				log.Fatal(err)
			}
		}()

		f, err := createPprofFile("cpu")
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()
		if err := pprof.StartCPUProfile(f); err != nil {
			log.Fatal(err)
		}
		defer pprof.StopCPUProfile()
	}

	if err := _main(); err != nil {
		log.Fatal(err)
	}
}

func _main() error {
	conn, err := grpc.Dial(*addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return err
	}
	defer conn.Close()
	c := pb.NewZeroClient(conn)

	s, err := c.Get(context.Background(), &pb.GetRequest{Total: int64(*total), Block: int32(*block)})
	if err != nil {
		return err
	}

	var n int64
	for err == nil {
		var resp *pb.GetReply
		resp, err = s.Recv()
		if err == nil {
			n += int64(len(resp.Data))
		}
	}
	if err == io.EOF {
		err = nil
	}
	log.Printf("received %d bytes", n)
	return err
}
